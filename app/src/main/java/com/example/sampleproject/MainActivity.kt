package com.example.sampleproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.sampleproject.Home.Home
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var mDelayHandler : Handler? = null
    private val TIME_DELAY : Long = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val animFadeIn : Animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bounce);
        sampletext.startAnimation(animFadeIn);

        mDelayHandler = Handler()

        mDelayHandler!!.postDelayed(mRunnable,TIME_DELAY)


    }

    internal val mRunnable : Runnable = Runnable{
        if(!isFinishing){
            val intent =Intent(this,Home::class.java)
            startActivity(intent)
            finish()
        }
    }

    public override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }



}